import './polyfills';
import ToggleMobileMenu from './components/ToggleMobileMenu';
import ToggleNoticeBar from './components/ToggleNoticeBar';
import Slider from './components/Slider';
import FooterList from './components/FooterList';

document.addEventListener('DOMContentLoaded', () => {
  ToggleNoticeBar();
  ToggleMobileMenu();
  Slider();
  if(window.innerWidth <= 768) {     
    FooterList();
  }
});
