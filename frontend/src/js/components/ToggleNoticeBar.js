export default () => {
  const noticeToggleBtns = document.querySelectorAll('.js-toggle-notice');

  const changeNoticeState = evt => {
    const parentNode = evt.target.closest('.notices__item');
    const content = parentNode.querySelector('.notices__content');
    const icon = parentNode.querySelector('.notices__icon');

    if(content.classList.contains('is-visible')) {
      icon.classList.add('--hide');
      content.classList.remove('is-visible');
    } else {
      icon.classList.remove('--hide');
      content.classList.add('is-visible');
    }   
  };

  noticeToggleBtns.forEach(button => {
    button.addEventListener('click', changeNoticeState);
  });  
};