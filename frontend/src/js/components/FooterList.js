export default () => {
  const toggler = document.querySelectorAll('.footer__mobile-toggler');

  const toggleContentBlock = (evt) => {
    const currentToggler = evt.target.closest('header');
    const parent = currentToggler.closest('.footer__navigate');
    const content = parent.querySelector('.footer__mobile-content');

    if(currentToggler.classList.contains('is-open')) {
      currentToggler.classList.remove('is-open');
      content.classList.remove('is-open');
    } else {
      currentToggler.classList.add('is-open');
      content.classList.add('is-open');
    }
  };

  toggler.forEach(it => it.addEventListener('click', toggleContentBlock));
};