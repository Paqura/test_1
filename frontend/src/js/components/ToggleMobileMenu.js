export default () => {
  const btnOpen = document.querySelector('.js-mobile-open');
  const btnClose = document.querySelector('.js-mobile-close');   
  const menu = document.querySelector('.mobile-menu');
  
  const toggleMobileMenu = () => {
    if(menu.classList.contains('is-open')) {      
      menu.classList.remove('is-open');
      return;
    }    
    menu.classList.add('is-open');
  };
  
  btnOpen.addEventListener('click', toggleMobileMenu);
  btnClose.addEventListener('click', toggleMobileMenu);
};

