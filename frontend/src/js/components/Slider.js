import Glide from '@glidejs/glide';

export default () => {
  const glide = new Glide('.glide');
  
  const setSlider = () => {
    glide.mount();
  };

  const dieSlider = () => {
    glide.destroy();
  };

  const isMobileWidth = () => {
    return window.innerWidth <= 768;
  };

  const setOrDieSlider = () => {
    if(isMobileWidth()) {
      return setSlider();
    }
    dieSlider();
  }; 
  
  setOrDieSlider();

  window.addEventListener('resize', setOrDieSlider);
};